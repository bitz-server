bitz-server (2.0.3-1) unstable; urgency=medium

  * New upstream release.
    - Refresh symbols files.
  * Declare compliance with Debian Policy 4.3.0.1 (No changes needed).
  * debian/copyright:
    - Bump years to 2019.

 -- Jörg Frings-Fürst <debian@jff.email>  Sat, 12 Jan 2019 16:57:03 +0100

bitz-server (2.0.1-1) unstable; urgency=medium

  * New upstream release:
    - Remove upstream applied patches 0600-man_page_spelling.patch and
      0100-spelling.patch
  * debian/rules:
    - Add "export DEB_CXXFLAGS_MAINT_APPEND  = -fPIE" and
      "export DEB_LDFLAGS_MAINT_APPEND = -fPIE -pie" to fix missing CXXFLAGS
      and LDFLAGS.

 -- Jörg Frings-Fürst <debian@jff.email>  Sun, 05 Aug 2018 00:14:26 +0200

bitz-server (2.0.0-1) unstable; urgency=medium

  * New upstream release:
    - Switch from log4cpp to spdlog.
  * Migrate to debhelper 11:
    - Change debian/compat to 11.
    - Bump minimum debhelper version in debian/control to >= 11.
  * Declare compliance with Debian Policy 4.1.5 (No changes needed).
  * debian/control:
    - Switch Vcs-* to new location.
    - Change Build-Depends from liblog4cpp5-dev to libspdlog-dev.
  * debian/rules:
    - Switch from deprecated dh_systemd_enable to dh_installsystemd.
  * debian/copyright:
    - Use secure copyright format URI.
    - Add new sections for lib/spdlog.
    - Bump years to 2018.
  * New debian/patches/0100-spelling.patch.
  * Remove debian/bitz-server-doc.docs to prevent twin install of README.md.

 -- Jörg Frings-Fürst <debian@jff.email>  Tue, 10 Jul 2018 20:15:54 +0200

bitz-server (1.0.2-1) unstable; urgency=medium

  * New upstream release:
    - Make builds reproducible (Closes: #882112).
  * Change to my new mail address:
    - debian/control,
    - debian/copyright
  * debian/copyright: Bump years to 2017.
  * Declare compliance with Debian Policy 4.1.2.0. (No changes needed).
  * debian/libicap1.symbols.ppc64el (Closes: #878847, #878844):
    - Mark symbols optional not emitted when building with -O3.
      Thanks to Matthias Klose <doko@ubuntu.com>.

 -- Jörg Frings-Fürst <debian@jff.email>  Sat, 16 Dec 2017 10:56:20 +0100

bitz-server (1.0.0-5) unstable; urgency=medium

  * Renew symbols files.

 -- Jörg Frings-Fürst <debian@jff-webhosting.net>  Sun, 15 Oct 2017 20:33:37 +0200

bitz-server (1.0.0-4) unstable; urgency=medium

  * Add missing symbols files.
  * New README.source to explain the branching model used.
  * Declare compliance with Debian Policy 4.1.1. (No changes needed).
  * Migrate to debhelper 10:
    - Change debian/compat to 10.
    - Bump minimum debhelper version in debian/control to >= 10.
    - Remove dh-systemd from Build-Depends.
    - Remove now useless autoreconf and "with systemd" from debian/rules.
    - Drop dh-autoreconf from both Build-Depends.
  * debian/control:
    - Add missing dependency lsb-base.
    - Add "Section: libs" to package libicap1.
  * Use the automatic debug symbol packages:
    - Remove bitz-server-dbg and libicap1-dbg section from debian/control.
    - Remove override_dh_strip from debian/rules.
  * Rebuild debian/libicap1.symbols.[i386|amd64] to fix a FTBFS
    (Closes: #853331).
  * Remove outdated debian/bitz-server.upstart.
  * New debian/maintscript to remove obsolete /etc/init/bitz-server.conf.

 -- Jörg Frings-Fürst <debian@jff-webhosting.net>  Sat, 14 Oct 2017 18:32:00 +0200

bitz-server (1.0.0-3) unstable; urgency=medium

  * Rebuild debian/libicap1.symbols.i386 to fix a FTBFS (Closes: #860700).

 -- Jörg Frings-Fürst <debian@jff-webhosting.net>  Wed, 19 Apr 2017 13:49:05 +0200

bitz-server (1.0.0-2) unstable; urgency=medium

  * Rename debian/libicab1.symbols to debian/libicab1.symbols.amd64
    to prevend builderrors on many architectures.
  * Add regex to debian/libicab1.symbols.amd64 to ignore
    std:: template instantiations (Closes: #812140).
  * Correct Vcs-Git uri at debian/control.

 -- Jörg Frings-Fürst <debian@jff-webhosting.net>  Sun, 03 Apr 2016 17:39:15 +0200

bitz-server (1.0.0-1) unstable; urgency=medium

  * New upstream release:
    - Move libicap0 to libicap1.
    - Remove 0100-python_print.patch.
  * debian/copyright:
    - Remove unused items.
    - Add year 2016 to debian/*.
  * debian/watch:
    - bump version to 4 (no changes required).
  * debian/control:
    - Change VCS-* to secure uri.
    - Correct typo.
    - Bump Standards-Version to 3.9.7 (no changes required).
  * New debian/patches/0600-man_page_spelling.patch.
  * Replace libicap0-dbg with libicap1-dbg.

 -- Jörg Frings-Fürst <debian@jff-webhosting.net>  Thu, 11 Feb 2016 20:00:56 +0100

bitz-server (0.1.6-1) unstable; urgency=low

  * Initial release (Closes: #715022)

 -- Jörg Frings-Fürst <debian@jff-webhosting.net>  Sat, 21 Nov 2015 15:21:43 +0100
